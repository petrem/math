module Polydivisible where
import Control.Monad (replicateM)
import Data.List (foldl', inits, nub)


type Digits = [Int]

-- this is extremely inefficient
numbersOfNUniqueDigits :: Int -> Int -> [Digits]
numbersOfNUniqueDigits base n = [xs | xs <- replicateM n [1..base], nub xs == xs]

polydivisible :: Int -> [Digits]
polydivisible base = [xs | xs <- numbersOfNUniqueDigits base base, isPolydivisible xs]

isPolydivisible :: Digits -> Bool
isPolydivisible = all isDivisibleByLength . tail . inits

isDivisibleByLength :: Digits -> Bool
isDivisibleByLength = (==0) . (rem <$> intFromDigits <*> length)

intFromDigits :: Digits -> Int
intFromDigits = foldl' (\n d -> n*10 + d) 0 
