-- | numbers that are (or not) the sum of sequential numbers

module Adjacent where

import Data.List (concatMap, splitAt, sort, filter, dropWhile, unfoldr)


adjacentSumsWithList :: Int -> [(Int, [Int])]
adjacentSumsWithList = sort . (((,) <$> sum <*> id) <$>) . adjacentsUpTo

adjacentSums :: Int -> [Int]
adjacentSums = uniq . (fst <$>) . adjacentSumsWithList

adjacentsUpTo :: Int -> [[Int]]
adjacentsUpTo = (uncurry enumFromTo <$>) . (toPair <$>) . subsequencesOfSize 2 . enumFromTo 1

sumsOfConsecutives :: Int -> Int -> [Int]
sumsOfConsecutives from to = filter (isBetween from to) (adjacentSums to)

notSumsOfConsecutives :: Int -> Int -> [Int]
notSumsOfConsecutives from to = filter (`notElem` sumsOfConsecutives from to) [from..to]

subsequencesOfSize :: Int -> [a] -> [[a]]
subsequencesOfSize n xs = let l = length xs
                          in if n>l then [] else subsequencesBySize xs !! (l-n)

subsequencesBySize :: [a] -> [[[a]]]
subsequencesBySize [] = [[[]]]
subsequencesBySize (x:xs) = let next = subsequencesBySize xs
                            in zipWith (++) ([]:next) (map (map (x:)) next ++ [[]])


fromPair :: (a, a) -> [a]
fromPair (x, y) = [x, y]

toPair :: [a] -> (a, a)
toPair [x, y] = (x, y)

isBetween :: Ord a => a -> a -> a -> Bool
isBetween a b = (&&) <$> (a <=) <*> (<= b)

uniq :: Eq a => [a] -> [a]
uniq = unfoldr go
  where go [] = Nothing
        go (x:xs) = Just (x, dropWhile (== x) xs)
        
