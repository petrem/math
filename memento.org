#+STARTUP: noentitiespretty hidestars indent nolatexpreview logdrawer
#+OPTIONS: tex:t
#+LATEX_HEADER: \usepackage{esdiff}

* Memento
** LaTeX (in Orgmode)
*** Environments
- math includes eg. =$...$=, =$$ ... $$=
- environments (i.e. =\begin{...} ... \end{...}=) such as =math=, =equation=, but also
  tables and so on
  
*** Keyboard Shortcuts
- =C-c C-x C-l= :: (org-latex-preview)
  - =C-u= ~ :: clear preview for all fragments in current section
  - =C-u C-u= ~ :: display preview for all fragments in buffer 
  - =C-u C-u C-u= ~ :: clear preview for all fragments in buffer
- =<f6>= :: create latex previews in current section (my custom binding)
  
*** Unicode Symbols and LaTex equivalents

See also  https://en.wikipedia.org/wiki/List_of_mathematical_symbols_by_subject

| Unicode    | Meaning                 | Preview                                          | LaTeX                                            | TeX Input (where diff) |
|------------+-------------------------+--------------------------------------------------+--------------------------------------------------+------------------------|
| ≡          | equivalence             | $\equiv$                                         | =\equiv=                                         |                        |
| →          | implication             | $\to$, $\rightarrow$                             | =\to=, =\rightarrow=                             |                        |
| ⇒          |                         | $\implies$, $\Longrightarrow$                    | =\implies=, =\Longrightarrow=                    | =\Longrightarrow=      |
| ⇒          |                         | $\Rightarrow$                                    | =\Rightarrow=                                    |                        |
| ↛ , ⇏      | not implies             | $\nrightarrow$, $\nRightarrow$                   | =\nrightarrow=, =\nRightarrow=                   |                        |
| ⇔          | iff                     | $\iff$, $\Leftrigharrow$                         | =\iff=, =\Leftrigharrow=                         |                        |
| ∃          | exists                  | $\exists$                                        | =\exists=                                        |                        |
| ∃!         | exists one and only one | $\exists!$                                       | =\exists!=                                       | =\exists= =!=          |
| ∄          | there is no             | $\nexists$                                       | =\nexists=                                       |                        |
| ∀          | for all                 | $\forall$                                        | =\forall=                                        |                        |
| ∈          | is element of           | $\in$                                            | =\in=                                            |                        |
| ∉          | is not element of       | $\notin$                                         | =\notin=                                         |                        |
| ℕ          | natural numbers         | $\mathbb{N}$                                     | =\mathbb{N}=                                     | =\Bbb{N}=              |
| ·          | multiplication (dot)    | $\cdot$                                          | =\cdot=                                          |                        |
| ×          | multiplication (cross)  | $\times$                                         | =\times=                                         |                        |
| ∩, ⋂       | intersection            | $\cap$, $\bigcap$                                | =\cap=, =\bigcap=                                |                        |
| ∪, ⋃       | union                   | $\cup$, $\bigcup$                                | =\cup=, =\bigcup=                                |                        |
| ⊂, ⊄, ⊆, ⊈ | subset                  | $\subset$, $\nsubset$, $\subseteq$, $\nsubseteq$ | =\subset=, =\nsubset=, =\subseteq=, =\nsubseteq= |                        |
| ⊃, ⊅, ⊇, ⊉ | superset                | $\supset$, $\nsupset$, $\supseteq$, $\nsupseteq$ | =\supset=, =\nsupset=, =\supseteq=, =\nsupseteq= |                        |
|            | complement              | $A^{\complement}$                                | =A^{\complement}=                                |                        |
| ◌̄A, ◌̅A    | (various)               | $\bar{A}$, $\overline{A}$                        | =\bar{A}=, =\overline{A}=                        |                        |
| ⟨, ⟩       | vector                  | $\left<a\right>$                                 | =\left<a\right>=                                 | =\langle=, =\rangle=   |
#+TBLFM: @2$4..@>$4 = '(string-replace "$" "=" $3)'

*** Math beyond symbols
